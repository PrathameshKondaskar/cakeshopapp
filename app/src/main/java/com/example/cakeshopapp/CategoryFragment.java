package com.example.cakeshopapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.cakeshopapp.activities.CategoryTabsActivity;
import com.example.cakeshopapp.models.Product;
import com.example.cakeshopapp.models.ProductModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;


public class CategoryFragment extends Fragment {

    public static final String KEY_PRODUCTS = "products";
    public static final String KEY_CART = "cart";
    ArrayList<ProductModel> productModels = new ArrayList<>();

    HashMap<String, Product> itemsHashMap = new HashMap<>();

    ListView listViewProducts;
    String category;
    View view;

    //INTERFACE
    private SendDataInterface sendDataInterface;


    @SuppressLint("ValidFragment")
    public CategoryFragment(ArrayList<ProductModel> productModels) {
        this.productModels = productModels;
    }

    public CategoryFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        sendDataInterface = ((CategoryTabsActivity) (getActivity()));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Gson gson = new Gson();
        productModels = gson.fromJson(getArguments().getString(KEY_PRODUCTS), new TypeToken<ArrayList<ProductModel>>() {
        }.getType());

        view = inflater.inflate(R.layout.fragment_category, container, false);
        listViewProducts = (ListView) view.findViewById(R.id.listViewProducts);
        category = getArguments().getString("category");
        CustomAdapter customAdapter = new CustomAdapter(getContext(), android.R.layout.simple_list_item_1, productModels);
        listViewProducts.setAdapter(customAdapter);


        return view;
    }

    public class CustomAdapter extends ArrayAdapter<ProductModel> {

        Context context;
        ArrayList<ProductModel> productModels;

        public CustomAdapter(Context context, int resource, ArrayList<ProductModel> productModels) {
            super(context, resource, productModels);
            this.context = context;
            this.productModels = productModels;
        }


        @Override
        public View getView(int position, View view, ViewGroup parent) {

            final Product product = new Product(productModels.get(position).getProductId(),
                    productModels.get(position).getProductName(),
                    productModels.get(position).getProductPrice(),
                    0);

            if (itemsHashMap.containsKey(product.getId())) {
                product.setQuantity(itemsHashMap.get(product.getId()).getQuantity());
            }

            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.custom_products, parent, false);

            TextView textViewItemName = (TextView) view.findViewById(R.id.textViewItemName);
            TextView textViewItemPrice = (TextView) view.findViewById(R.id.textViewItemPrice);
            ImageView imageViewProduct = (ImageView) view.findViewById(R.id.imageViewProduct);
            final TextView textViewAdd = (TextView) view.findViewById(R.id.textViewAdd);
            final TextView textViewRemove = (TextView) view.findViewById(R.id.textViewRemove);
            final TextView textViewQuantity = (TextView) view.findViewById(R.id.textViewQantity);

            textViewItemName.setText(productModels.get(position).getProductName());
            textViewItemPrice.setText(" Rs. " + productModels.get(position).getProductPrice());
            textViewQuantity.setText(product.getQuantity() + "");

            Glide.with(view)
                    .load(productModels.get(position).getImageURL())
                    .into(imageViewProduct);


            textViewAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    product.setQuantity(product.getQuantity() + 1);
                    textViewQuantity.setText("" + product.getQuantity());
                    sendDataInterface.getData(product);
                    itemsHashMap.put(product.getId(), product);
                }
            });

            textViewRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(product.getQuantity()>0) {
                        product.setQuantity(product.getQuantity() - 1);
                        textViewQuantity.setText("" + product.getQuantity());
                        sendDataInterface.getData(product);
                        itemsHashMap.put(product.getId(), product);
                    }
                }
            });


            return view;
        }
    }

    public interface SendDataInterface {

        public void getData(Product productModel);
    }
}
