package com.example.cakeshopapp.activities;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cakeshopapp.R;
import com.example.cakeshopapp.helpers.CustomLinearLayoutManager;
import com.example.cakeshopapp.models.OrderHistoryItemModel;
import com.example.cakeshopapp.models.PerItemOrder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

public class EmployeeOrderHistory extends AppCompatActivity {

    RecyclerView recyclerViewHistory;
    private ProgressBar progressBar;

    LinearLayoutManager linearLayoutManager;

    CustomOrderAdapter customOrderAdapter;
    ArrayList<OrderHistoryItemModel> orderHistoryItems;
    FirebaseFirestore db;
    String userId;
    FirebaseAuth auth;
    FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_order_history);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerViewHistory = (RecyclerView) findViewById(R.id.historyList);
        linearLayoutManager = new LinearLayoutManager(this);

        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);


        recyclerViewHistory.setLayoutManager(linearLayoutManager);

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        //userId = user.getUid();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        db = FirebaseFirestore.getInstance();
        progressBar.setVisibility(View.VISIBLE);

        getOrdersFromFirebase();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getOrdersFromFirebase() {

        db.collection("Orders")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        progressBar.setVisibility(View.GONE);

                        orderHistoryItems = new ArrayList<>();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            if (document.getData().get("status").toString().matches("REJECTED" ) || document.getData().get("status").toString().matches("DELIVERED" )) {
                                OrderHistoryItemModel orderHistoryItemModel = document.toObject(OrderHistoryItemModel.class);
                                orderHistoryItemModel.setDocId(document.getId());
                                orderHistoryItems.add(orderHistoryItemModel);
                            }

                        }
                        customOrderAdapter = new CustomOrderAdapter(EmployeeOrderHistory.this, orderHistoryItems);

                        recyclerViewHistory.setAdapter(customOrderAdapter);


                    }
                });


    }

    public class CustomOrderAdapter extends RecyclerView.Adapter<CustomOrderAdapter.ViewHolder> {

        private ArrayList<OrderHistoryItemModel> orderHistoryItems;
        private Context context;

        public CustomOrderAdapter(Context context, ArrayList<OrderHistoryItemModel> orderHistoryItems) {
            this.orderHistoryItems = orderHistoryItems;
            this.context = context;
        }

        @NonNull
        @Override
        public CustomOrderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_order_history, parent, false);
            return new ViewHolder(itemView);

        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {



            SelectedItemsRecyclerAdapter selectedItemsRecyclerAdapter = new SelectedItemsRecyclerAdapter(orderHistoryItems.get(position).getPerItemOrder());
            RecyclerView.LayoutManager layoutManager = new CustomLinearLayoutManager(context);
            holder.mSelctedItemsList.setLayoutManager(layoutManager);
            holder.mSelctedItemsList.setAdapter(selectedItemsRecyclerAdapter);

            //holder.mTapriName.setText(data[position].getTapriId().getName());
            holder.mOrderId.setText(orderHistoryItems.get(position).getOrderId().toUpperCase());
            holder.mOrderStatus.setText(orderHistoryItems.get(position).getStatus().toUpperCase());
            String timestamp = orderHistoryItems.get(position).getDate();
            holder.mDate.setText(timestamp.substring(0,timestamp.indexOf('T')));

            holder.mTotalCost.setText("₹"+orderHistoryItems.get(position).getTotal());
            holder.mName.setText(orderHistoryItems.get(position).getName());
            holder.mPlacedOn.setText(orderHistoryItems.get(position).getPlacedOn());
            holder.mAddress.setText(orderHistoryItems.get(position).getAddress());
            holder.mPaymentMethod.setText(orderHistoryItems.get(position).getPaymentMethod());

            if(position==orderHistoryItems.size()-1){
                holder.mBody.setVisibility(View.VISIBLE);
                holder.mOrderId.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_drop_up_black_24dp, 0, 0, 0);
            }


        }

        @Override
        public int getItemCount() {
            return orderHistoryItems.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {


            public TextView mTotalCost;
            public TextView mAddress;
            public TextView mPaymentMethod;
            public RecyclerView mSelctedItemsList;
            public Switch mRecivedSwitch;
            public TextView mOrderId;
            public TextView mOrderStatus;
            public LinearLayout mHeader;
            public LinearLayout mBody;
            public TextView mDate;
            public TextView mName;
            public TextView mPlacedOn;


            public ViewHolder(View itemView) {
                super(itemView);

                mTotalCost = itemView.findViewById(R.id.totalcost);
                mAddress = itemView.findViewById(R.id.addressall);
                mPaymentMethod = itemView.findViewById(R.id.paymentmethod);
                mSelctedItemsList = itemView.findViewById(R.id.selected_items_list);
                mOrderId = itemView.findViewById(R.id.orderid);
                mOrderStatus = itemView.findViewById(R.id.orderstatus);
                mHeader = itemView.findViewById(R.id.order_header);
                mBody = itemView.findViewById(R.id.order_footer);
                mDate = itemView.findViewById(R.id.orderdate);
                mName = itemView.findViewById(R.id.tvName);
                mPlacedOn = itemView.findViewById(R.id.tvDate);

                mHeader.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mBody.getVisibility()== View.GONE){
                            mBody.setVisibility(View.VISIBLE);
                            mOrderId.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_drop_up_black_24dp, 0, 0, 0);


                        }else {
                            mBody.setVisibility(View.GONE);
                            mOrderId.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_drop_down_black_24dp, 0, 0, 0);
                        }
                    }
                });
            }


        }
    }

    public class SelectedItemsRecyclerAdapter extends RecyclerView.Adapter<SelectedItemsRecyclerAdapter.ViewHolder> {

        public List<PerItemOrder> perItemOrders;

        public SelectedItemsRecyclerAdapter(List<PerItemOrder> perItemOrders) {
            this.perItemOrders = perItemOrders;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_selected_item, parent, false);

            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull SelectedItemsRecyclerAdapter.ViewHolder holder, int position) {

            if (perItemOrders != null) {
                holder.name.setText(perItemOrders.get(position).getName());
                holder.quant.setText(perItemOrders.get(position).getQuantity() + "");
                holder.rate.setText("₹" + perItemOrders.get(position).getPricePerItem());
            }
        }

        @Override
        public int getItemCount() {
            return perItemOrders.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView name;
            public TextView quant;
            public TextView rate;

            public ViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.item_name);
                quant = (TextView) itemView.findViewById(R.id.item_quant);
                rate = (TextView) itemView.findViewById(R.id.item_rate);
            }
        }
    }


}
