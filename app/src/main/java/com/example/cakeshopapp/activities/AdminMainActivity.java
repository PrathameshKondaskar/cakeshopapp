package com.example.cakeshopapp.activities;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.example.cakeshopapp.R;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.cakeshopapp.models.ProductModel;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public class AdminMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    EditText editTextProductName, editTextProductPrice;
    Spinner spinnerCategory;
    Button buttonAddProduct;
    String productId;
    ImageView imageViewProduct,imageViewProduct1;
    ArrayList<String> categories;
    String category;

    Bitmap bm;
    int flag = 0;
    String imgurl;
    private static final int PICK_IMAGE_REQUEST = 1;
        private Uri mImageUri;

    ProgressDialog progressDialog;

    //Firebase
    FirebaseFirestore mFirestore;
    private StorageReference mStorageRef;
    private StorageTask mUploadTask;

    String timeStamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mFirestore = FirebaseFirestore.getInstance();
        mStorageRef= FirebaseStorage.getInstance().getReference();

        progressDialog = new ProgressDialog(this);
        editTextProductName = (EditText) findViewById(R.id.editTextProductName);
        editTextProductPrice = (EditText) findViewById(R.id.editTextProductPrice);
        imageViewProduct = (ImageView) findViewById(R.id.imageViewProduct);
        imageViewProduct1 = (ImageView) findViewById(R.id.imageViewProduct1);
        spinnerCategory = (Spinner) findViewById(R.id.spinnerCategory);
        buttonAddProduct = (Button)findViewById(R.id.buttonAddProduct);


        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'"); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);
        timeStamp = df.format(new Date());


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        categories = new ArrayList<>();
        categories.add("BIRTHDAY");
        categories.add("WEDDING");
        categories.add("FONDANT");
        categories.add("CUSTOMIZE");
        categories.add("PASTRIES");
        categories.add("CUP CAKE");
//        progressBar = (ProgressBar)findViewById(R.id.progressBar);


        CustomAdapter customAdapter = new CustomAdapter(AdminMainActivity.this, android.R.layout.simple_list_item_1, categories);
        spinnerCategory.setAdapter(customAdapter);

        imageViewProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });
        imageViewProduct1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });
        buttonAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProduct();
            }
        });
    }

    public void addProduct() {
        final String productId = randomNo();
        final String productName = editTextProductName.getText().toString();
        final String productPrice = editTextProductPrice.getText().toString();
        final String category = spinnerCategory.getSelectedItem().toString();

        boolean shouldCancelSignUp = false;
        View focusView = null;


        if (productName.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextProductName;
            editTextProductName.setError("name is a required field");
        }
        if (productPrice.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextProductPrice;
            editTextProductPrice.setError("address is a required field");
        }
        if (flag == 0) {
            Toast.makeText(this, "Image Not Selected", Toast.LENGTH_SHORT).show();
        } else if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else
        {
            progressDialog.setMessage("Adding Product...");
            progressDialog.show();
            progressDialog.setCancelable(false);



            final String path = System.currentTimeMillis()+"."+getExtenssion(mImageUri);
            final StorageReference fileRef=mStorageRef.child(path);


            fileRef.putFile(mImageUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()){
                        throw task.getException();
                    }
                    return fileRef.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()){
                        Uri downUri = task.getResult();
                        Log.d("PATH", "onComplete: Url: "+ downUri.toString());


                        ProductModel productModel = new ProductModel(productId,productName,productPrice,category,downUri.toString(),timeStamp);

                        mFirestore.collection("Products").add(productModel)
                                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentReference> task) {
                                        if(task.isSuccessful())
                                        {
                                            progressDialog.dismiss();
                                            Toast.makeText(AdminMainActivity.this, "Product added", Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(AdminMainActivity.this,AdminMainActivity.class));
                                            finish();
                                        }
                                        else
                                        {
                                            progressDialog.dismiss();
                                            Toast.makeText(AdminMainActivity.this, "Data not added", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }
                }
            });



        }


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.admin_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_ahome) {
startActivity(new Intent(AdminMainActivity.this,AdminMainActivity.class));
finish();
        } else if (id == R.id.nav_ahistory) {
            startActivity(new Intent(AdminMainActivity.this,EmployeeOrderHistory.class));

        }
        else if (id == R.id.nav_alogout) {
            startActivity(new Intent(AdminMainActivity.this,LoginActivity.class));
            finish();

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class CustomAdapter extends ArrayAdapter<String> implements SpinnerAdapter {
        Context context;
        ArrayList<String> paymentMode = new ArrayList<>();

        public CustomAdapter(@NonNull Context context, int resource, @NonNull ArrayList<String> objects) {
            super(context, resource, objects);
            this.context = context;
            this.paymentMode = objects;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


            View view = super.getView(position, convertView, parent);

            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setTextColor(getResources().getColor(R.color.black));
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            textView.setGravity(Gravity.CENTER);
            /*Typeface typeface = ResourcesCompat.getFont(context, R.font.source_sans_pro_semibold);
            textView.setTypeface(typeface);*/
            return view;
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            convertView = LayoutInflater.from(AdminMainActivity.this).inflate(
                    R.layout.custom_spinner, parent, false);
            TextView textView = (TextView) convertView.findViewById(R.id.text);
            textView.setText(paymentMode.get(position));

            return convertView;
        }
    }


    private void openFileChooser() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            mImageUri = data.getData();
            Log.d("path", mImageUri.toString());
            imageViewProduct.setImageURI(mImageUri);
            //Picasso.with(this).load(mImageUri).into(mImageView);
            //Bitmap bitmap = (Bitmap)data.getExtras().get(data.getData().toString());
            //mImageView.setImageBitmap(bitmap);
            imageViewProduct.buildDrawingCache();
            flag = 1;

        }
    }

    private String getExtenssion(Uri uri) {
        ContentResolver cr = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cr.getType(uri));

    }

    public String randomNo() {
        char[] chars1 = "ABCDEF012GHIJKL345MNOPQR678STUVWXYZ9".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 6; i++) {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        return sb1.toString();
    }



}
