package com.example.cakeshopapp.activities;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import com.example.cakeshopapp.R;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.cakeshopapp.helpers.DateFormatHelper;
import com.example.cakeshopapp.models.OrderHistoryItemModel;
import com.example.cakeshopapp.models.PerItemOrder;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

public class CustomerOrderHistoryActivity extends AppCompatActivity {


    //VIEWS
    private ListView listView;
    private ProgressBar progressBar;
    private ImageView imageViewHotelLogo;

    //VARIABLES
    CustomHistoryAdapter customHistoryAdapter;
    ArrayList<OrderHistoryItemModel> orderHistoryItems;
    FirebaseFirestore db;
    String userId;
    FirebaseAuth auth;
    FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);

getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        userId = user.getUid();
        listView = (ListView) findViewById(R.id.listViewOrderHistory);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        db = FirebaseFirestore.getInstance();
        progressBar.setVisibility(View.VISIBLE);
        getOrdersFromFirebase();
    }


    public void getOrdersFromFirebase() {
        db.collection("Orders")
                .whereEqualTo("userId", userId)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        progressBar.setVisibility(View.GONE);

                        orderHistoryItems = new ArrayList<>();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            OrderHistoryItemModel orderHistoryItemModel = document.toObject(OrderHistoryItemModel.class);
                            orderHistoryItemModel.setDocId(document.getId());
                            orderHistoryItems.add(orderHistoryItemModel);

                        }
                        customHistoryAdapter = new CustomHistoryAdapter(CustomerOrderHistoryActivity.this, android.R.layout.simple_list_item_1, orderHistoryItems);

                        listView.setAdapter(customHistoryAdapter);
                    }
                });
    }

    public class CustomHistoryAdapter extends ArrayAdapter<OrderHistoryItemModel> {
        View view;
        List<OrderHistoryItemModel> orderHistoryItems;

        public CustomHistoryAdapter(Context context, int resource, List<OrderHistoryItemModel> orderHistoryItems) {
            super(context, resource, orderHistoryItems);
            this.orderHistoryItems = orderHistoryItems;
        }


        @Override
        public View getView(final int position, View view, ViewGroup parent) {

            TextView textViewOrderId, textViewItem, textViewTimeStamp, textViewTotal, textViewStatus1, textViewStatus2, textViewStatus3, textViewCurrentStatus;
            ImageView imageView1,imageView2,imageView3,imageView4;
            Switch switchRecived;

            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.custom_order_history_list, parent, false);

            switchRecived = (Switch)view.findViewById(R.id.switchRecived);
            imageView1 = (ImageView)view.findViewById(R.id.imageView1);
            imageView2 = (ImageView)view.findViewById(R.id.imageView2);
            imageView3 = (ImageView)view.findViewById(R.id.imageView3);
            imageView4 = (ImageView)view.findViewById(R.id.imageView4);

            textViewOrderId = (TextView)view. findViewById(R.id.textViewOrderId);
            textViewItem = (TextView)view. findViewById(R.id.textViewItems);
            textViewTimeStamp = (TextView)view. findViewById(R.id.textViewTimeStamp);
            textViewTotal = (TextView)view. findViewById(R.id.textViewTotal);
            textViewStatus1 = (TextView)view. findViewById(R.id.textViewStatus1);
            textViewStatus2 = (TextView) view.findViewById(R.id.textViewStatus2);
            textViewStatus3 = (TextView)view. findViewById(R.id.textViewStatus3);
            textViewCurrentStatus = (TextView)view. findViewById(R.id.textViewCurrentStatus);

            textViewStatus1.setPaintFlags(textViewStatus1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            textViewStatus2.setPaintFlags(textViewStatus1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            textViewStatus3.setPaintFlags(textViewStatus1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            textViewOrderId.setText(orderHistoryItems.get(position).getOrderId());

            if(orderHistoryItems.get(position).getStatus().matches("PLACED"))
            {

                imageView2.setImageResource(R.drawable.successgrey);
                imageView3.setImageResource(R.drawable.successgrey);
                imageView4.setImageResource(R.drawable.successgrey);

            }



            if(orderHistoryItems.get(position).getStatus().matches("ACCEPTED"))
            {

                imageView3.setImageResource(R.drawable.successgrey);
                imageView4.setImageResource(R.drawable.successgrey);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    textViewStatus1.setBackgroundColor(Color.parseColor("#FFFCE28F"));
                }
                textViewStatus1.setPaintFlags(textViewStatus1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            }
            if(orderHistoryItems.get(position).getStatus().matches("PREPARED"))
            {
                imageView4.setImageResource(R.drawable.successgrey);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    textViewStatus2.setBackgroundColor(Color.parseColor("#FFFCE28F"));
                    textViewStatus1.setBackgroundColor(Color.parseColor("#FFFCE28F"));

                }
                textViewStatus2.setPaintFlags(textViewStatus2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                textViewStatus1.setPaintFlags(textViewStatus2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
            if(orderHistoryItems.get(position).getStatus().matches("DELIVERED"))
            {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    textViewStatus2.setBackgroundColor(Color.parseColor("#FFFCE28F"));
                    textViewStatus1.setBackgroundColor(Color.parseColor("#FFFCE28F"));
                    textViewStatus3.setBackgroundColor(Color.parseColor("#FFFCE28F"));
                }
                textViewStatus3.setPaintFlags(textViewStatus3.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                textViewStatus2.setPaintFlags(textViewStatus2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                textViewStatus1.setPaintFlags(textViewStatus2.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            }

            if(orderHistoryItems.get(position).getStatus().toUpperCase().equals("PREPARED".toUpperCase())){
                switchRecived.setVisibility(View.VISIBLE);
            }
            if(orderHistoryItems.get(position).getStatus().toUpperCase().equals("DELIVERED".toUpperCase())){
                switchRecived.setVisibility(View.GONE);
            }
            if(orderHistoryItems.get(position).getStatus().toUpperCase().equals("REJECTED".toUpperCase())){
               imageView1.setVisibility(View.GONE);
               imageView2.setVisibility(view.GONE);
                imageView3.setVisibility(View.GONE);
                imageView4.setVisibility(view.GONE);

                textViewStatus1.setVisibility(View.GONE);
                textViewStatus2.setVisibility(View.GONE);
                textViewStatus3.setVisibility(View.GONE);
            }


            switchRecived.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isCheck) {
                    if(isCheck)
                    {
                        Map<String, Object> map = new HashMap<>();
                        map.put("status", "DELIVERED");


                        db.collection("Orders")
                                .document(orderHistoryItems.get(position).getDocId())
                                .update(map)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                    }
                                });

                    }
                }
            });


            String date = DateFormatHelper.getDisplayableDate(orderHistoryItems.get(position).getDate());
            textViewTimeStamp.setText(date);

            textViewTotal.setText("Rs. "+orderHistoryItems.get(position).getTotal());
            textViewCurrentStatus.setText(orderHistoryItems.get(position).getStatus());

            ArrayList<String> items = new ArrayList<>();
            for (PerItemOrder perItemOrder : orderHistoryItems.get(position).getPerItemOrder()) {
                items.add(perItemOrder.getQuantity() + " × " + perItemOrder.getName());
            }
            String result = TextUtils.join(", ", items);
            textViewItem.setText(result);


            return view;

        }


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
