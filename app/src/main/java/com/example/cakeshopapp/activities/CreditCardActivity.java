package com.example.cakeshopapp.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.example.cakeshopapp.R;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

public class CreditCardActivity extends AppCompatActivity {

    EditText editTextCardNo, editTextMobile, editTextCVV, editTextExpiry, editTextPostal;
    Button buttonSubmit;
    AlertDialog dialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        editTextCardNo = (EditText) findViewById(R.id.editTextCardNo);
        editTextMobile = (EditText) findViewById(R.id.editTextMobile);
        editTextCVV = (EditText) findViewById(R.id.editTextCVV);
        editTextExpiry = (EditText) findViewById(R.id.editTextExpiry);
        editTextPostal = (EditText) findViewById(R.id.editTextPostal);
        buttonSubmit = (Button) findViewById(R.id.buttonSubmit);

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toPay();
            }
        });

    }

    public void toPay() {
        String card = editTextCardNo.getText().toString();
        String mobile = editTextMobile.getText().toString();
        String cvv = editTextCVV.getText().toString();
        String expiry = editTextExpiry.getText().toString();
        String postal = editTextPostal.getText().toString();

        boolean shouldCancelSignUp = false;
        View focusView = null;


        if (card.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextCardNo;
            editTextCardNo.setError("Card no. is a required field");
        }
        if (cvv.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextCVV;
            editTextCVV.setError("CVV is a required field");

        }
        if (expiry.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextExpiry;
            editTextExpiry.setError("Expiry date is a required field");

        }
        if (postal.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextPostal;
            editTextPostal.setError("Postal Code date is a required field");
        }

        if (mobile.equals("")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. is a required field");
        }
        if (!mobile.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. is a invalid");

        }
        if (mobile.length() != 10 || mobile.matches(".*[a-z].*")) {
            shouldCancelSignUp = true;
            focusView = editTextMobile;
            editTextMobile.setError("mobile no. must be of 10 digit");
        }
        if (card.length() > 16 || card.length() < 16) {
            shouldCancelSignUp = true;
            focusView = editTextCardNo;
            editTextCardNo.setError("Card no. must be 16 digit");
        }
        if (cvv.length() > 3 || cvv.length() < 3) {
            shouldCancelSignUp = true;
            focusView = editTextCVV;
            editTextCVV.setError("CVV must be 3 digit");
        }
        if (postal.length() < 6 || postal.length() > 6) {
            shouldCancelSignUp = true;
            focusView = editTextPostal;
            editTextPostal.setError("Postal Code must be 6 digit");
        }

        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            LayoutInflater inflater = (LayoutInflater) CreditCardActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final AlertDialog.Builder mBuild = new AlertDialog.Builder(CreditCardActivity.this);
            final View mv = inflater.inflate(R.layout.content_progress, null);
            final ProgressBar progressBar = (ProgressBar) mv.findViewById(R.id.progress);
            final Button buttonOk = (Button) mv.findViewById(R.id.buttonOk);
            final TextView textViewOrderPlacing = (TextView) mv.findViewById(R.id.textViewOrderPlacing);
            final TextView textViewSuccess = (TextView) mv.findViewById(R.id.textViewSuccess);
            textViewOrderPlacing.setVisibility(View.VISIBLE);
            buttonOk.setVisibility(View.GONE);
            mBuild.setView(mv);
            mBuild.setCancelable(false);
            dialog = mBuild.create();
            dialog.show();
            dialog.setCanceledOnTouchOutside(false);
            final Handler handler = new Handler();
            final Timer timer = new Timer();
            final Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    textViewOrderPlacing.setVisibility(View.GONE);
                    textViewSuccess.setVisibility(View.VISIBLE);
                    buttonOk.setVisibility(View.VISIBLE);
                    timer.cancel();
                    buttonOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            Intent intent1 = new Intent(CreditCardActivity.this, MainActivity.class);
                            startActivity(intent1);
                            setResult(100, intent1);
                            finish();
                        }
                    });

                }
            };

            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(runnable);
                }
            }, 4000, 1000);


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
