package com.example.cakeshopapp.activities;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.cakeshopapp.R;
import com.example.cakeshopapp.UserSharedPreference;
import com.example.cakeshopapp.models.UserInfoModel;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    int flag = 0;
    String imgurl;
    private static final int PICK_IMAGE_REQUEST = 1;
    private Uri mImageUri = null;


    // Initialize Firebase Auth
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFireStore;
    UserInfoModel userInfo;
    UserSharedPreference userSharedPreference;
    private StorageReference mStorageRef;
    private ArrayList<UserInfoModel> userInfoModelList = new ArrayList<>() ;

    //Views
    private ImageView imageView,backButton;
    private Button submit;
    private TextInputEditText email , address  , mobile , name;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        imageView = findViewById(R.id.imageProfile);
        submit = findViewById(R.id.buttonSubmit);
        backButton = findViewById(R.id.backButton);
        userSharedPreference = new UserSharedPreference(this);
        progressDialog= new ProgressDialog(this);
        email = findViewById(R.id.email);
        address = findViewById(R.id.address);
        mobile = findViewById(R.id.mobile);
        name = findViewById(R.id.name);
        mAuth = FirebaseAuth.getInstance();
        mFireStore = FirebaseFirestore.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();

        getProfileData();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfile();
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileChooser();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void openFileChooser() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            mImageUri = data.getData();
            Log.d("path", mImageUri.toString());
            imageView.setImageURI(mImageUri);
            //Picasso.with(this).load(mImageUri).into(mImageView);
            //Bitmap bitmap = (Bitmap)data.getExtras().get(data.getData().toString());
            //mImageView.setImageBitmap(bitmap);
            imageView.buildDrawingCache();
            flag = 1;

        }
    }

    private String getExtenssion(Uri uri) {
        ContentResolver cr = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cr.getType(uri));

    }

    public void updateProfile(){
try {
    final String userEmail = email.getText().toString().trim();
    final String userAddress = address.getText().toString();
    final String userMobile = mobile.getText().toString();
    final String userName = name.getText().toString();

    boolean shouldCancelSignUp = false;
    View focusView = null;

    if (userName.equals("")) {
        shouldCancelSignUp = true;
        focusView = name;
        name.setError("name is a required field");
    }
    if (userEmail.equals("")) {
        shouldCancelSignUp = true;
        focusView = email;
        email.setError("email is a required field");
    }
    if (!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
        shouldCancelSignUp = true;
        focusView = email;
        email.setError("email is not valid");
    }

    if (userAddress.equals("")) {
        shouldCancelSignUp = true;
        focusView = address;
        address.setError("Address is a required field");
    }

    if (userMobile.equals("")) {
        shouldCancelSignUp = true;
        focusView = mobile;
        mobile.setError("mobile no. is a required field");
    }
    if (!userMobile.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$")) {
        shouldCancelSignUp = true;
        focusView = mobile;
        mobile.setError("mobile no. is a invalid");

    }
    if (mobile.length() != 10 || userMobile.matches(".*[a-z].*")) {
        shouldCancelSignUp = true;
        focusView = mobile;
        mobile.setError("mobile no. must be of 10 digit");
    }


    if (shouldCancelSignUp) {
        // There was an error; don't attempt login and focus the first
        // form field with an error.
        focusView.requestFocus();
    } else {

        progressDialog.setMessage("Updating Profile...");
        progressDialog.show();
        progressDialog.setCancelable(false);


        if (mImageUri != null) {
            final String path = System.currentTimeMillis() + "." + getExtenssion(mImageUri);
            final StorageReference fileRef = mStorageRef.child("Profile pictures").child(path);
            fileRef.putFile(mImageUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return fileRef.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {

                    Uri downUri = task.getResult();
                    Log.d("PATH", "onComplete: Url: " + downUri.toString());


//                            Toast.makeText(getActivity(), "Done done", Toast.LENGTH_SHORT).show();
//                            progressDialog.dismiss();


                    Map<String, Object> map = new HashMap<>();
                    map.put("name", userName);
                    map.put("address", userAddress);
                    map.put("email", userEmail);
                    map.put("mobile", userMobile);
                    map.put("imageUri", downUri.toString());

                    mFireStore
                            .collection(UserInfoModel.FIREBASE_COLLECTION_USERINFO)
                            .document(mAuth.getUid())
                            .update(map)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    progressDialog.dismiss();
                                    userSharedPreference.setAddress(userAddress);
                                    userSharedPreference.setMobile(userMobile);
                                    userSharedPreference.setName(userName);
                                    Toast.makeText(ProfileActivity.this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                                }
                            }).

                            addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("TAG", "onFail: "+e.getMessage());
                            progressDialog.dismiss();
                        }
                    });


                }
            });

        } else {

            Map<String, Object> map = new HashMap<>();
            map.put("name", userName);
            map.put("address", userAddress);
            map.put("email", userEmail);
            map.put("mobile", userMobile);

            mFireStore
                    .collection(UserInfoModel.FIREBASE_COLLECTION_USERINFO)
                    .document(mAuth.getUid())
                    .update(map)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            userSharedPreference.setAddress(userAddress);
                            userSharedPreference.setMobile(userMobile);
                            userSharedPreference.setMobile(userName);
                            progressDialog.dismiss();
                            Toast.makeText(ProfileActivity.this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                        }
                    })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d("TAG", "onFail: "+e.getMessage());
                }
            });
        }
    }
}catch (Exception e){
    Log.d("TAG", "updateProfile: "+e.getMessage());
}


    }

    public void getProfileData(){
        progressDialog.setMessage("Loading Profile...");
        progressDialog.show();
        progressDialog.setCancelable(false);


      mFireStore.collection(UserInfoModel.FIREBASE_COLLECTION_USERINFO)
              .whereEqualTo("userId",mAuth.getUid())
              .get()
              .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                  @Override
                  public void onSuccess(QuerySnapshot querySnapshot) {
                      if(!querySnapshot.isEmpty()) {

                          try {
                              progressDialog.dismiss();
                              List<UserInfoModel> types = querySnapshot.toObjects(UserInfoModel.class);
                              userInfoModelList.addAll(types);

                              UserInfoModel userInfoModel = userInfoModelList.get(0);

                              email.setText(userInfoModel.getEmail());
                              address.setText(userInfoModel.getAddress());
                              mobile.setText(userInfoModel.getMobile());
                              name.setText(userInfoModel.getName());

                              if (!userInfoModel.getImageUri().equalsIgnoreCase("")) {
                                  Glide.with(ProfileActivity.this)
                                          .load(userInfoModel.getImageUri())
                                          .into(imageView);
                              }
                          }catch (Exception e){
                              Log.d("TAG", "onSuccess: "+e.getMessage());
                          }

                      }
                  }
              }).addOnFailureListener(new OnFailureListener() {
          @Override
          public void onFailure(@NonNull Exception e) {

              progressDialog.dismiss();
              Toast.makeText(ProfileActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
          }
      });


    }

}