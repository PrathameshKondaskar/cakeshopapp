package com.example.cakeshopapp.models;

import java.util.List;

public class CategoryWiseOrder {
    String category;
    List<PerItemOrder> perItemOrder;

    public CategoryWiseOrder(String category, List<PerItemOrder> perItemOrder) {
        this.category = category;
        this.perItemOrder = perItemOrder;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<PerItemOrder> getPerItemOrder() {
        return perItemOrder;
    }

    public void setPerItemOrder(List<PerItemOrder> perItemOrder) {
        this.perItemOrder = perItemOrder;
    }
}
