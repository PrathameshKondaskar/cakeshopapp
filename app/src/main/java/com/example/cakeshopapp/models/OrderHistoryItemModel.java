package com.example.cakeshopapp.models;

import java.util.ArrayList;

public class OrderHistoryItemModel {

    String docId,orderId,paymentMethod,address,status,userId,date,mobile,name,placedOn;
    int total;
    ArrayList<PerItemOrder> perItemOrder;
    String token;

    public OrderHistoryItemModel(String docId, String orderId, String paymentMethod, String address, String status, String userId, String date, String mobile, int total, ArrayList<PerItemOrder> perItemOrder, String token,String name,String  placedOn) {
        this.docId = docId;
        this.orderId = orderId;
        this.paymentMethod = paymentMethod;
        this.address = address;
        this.status = status;
        this.userId = userId;
        this.date = date;
        this.mobile = mobile;
        this.total = total;
        this.perItemOrder = perItemOrder;
        this.token = token;
        this.name = name;
        this.placedOn = placedOn;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public OrderHistoryItemModel() {
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ArrayList<PerItemOrder> getPerItemOrder() {
        return perItemOrder;
    }

    public void setPerItemOrder(ArrayList<PerItemOrder> perItemOrder) {
        this.perItemOrder = perItemOrder;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlacedOn() {
        return placedOn;
    }

    public void setPlacedOn(String placedOn) {
        this.placedOn = placedOn;
    }
}
